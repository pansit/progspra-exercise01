/*
 * Bei der Initialisierung wird ein neues Objekt eines Datentyps oder einer
 * Klasse zum ersten mal erzeugt.
 *
 * Bei einer Zuweisung wird einem existierenden Objekt ein anderer Wert
 * zugewiesen.
 * 
 */