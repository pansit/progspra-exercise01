#ifndef FUNCTIONS_CPP
#define FUNCTIONS_CPP

#include <cmath>

unsigned factorial(unsigned n)
{
  if(n == 0)
    return 1;
  return factorial(n - 1) * n;
}

int checksum(int n)
{
  int res = 0;
  while(n > 0)
  {
    res += n % 10;
    n /= 10;
  }
  return res;
}

bool isPrime(int value)
{
  if(value <= 3)
    return value > 1;

  for (int i = 3; i < value; ++i)
    if (value % i == 0)
      return false;
  return true;
}

double frac(double value)
{
  return fmod(value, 1.0);
}

double milesToKilometers(double value)
{
  return value * 1.609344;
}

#endif //FUNCTIONS_CPP