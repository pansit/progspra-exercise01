#ifndef CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_RUNNER

#include "catch.hpp"
#include <cmath>

unsigned factorial(unsigned n)
{
  if(n == 0)
    return 1;
  return factorial(n - 1) * n;
}

int checksum(int n)
{
  int res = 0;
  while(n > 0)
  {
    res += n % 10;
    n /= 10;
  }
  return res;
}

bool isPrime(int value)
{
  if(value <= 3)
    return value > 1;

  for (int i = 3; i < value; ++i)
    if (value % i == 0)
      return false;
  return true;
}

double fract(double value)
{
  return fmod(value, 1.0);
}

double volume(double radius, double height)
{
  return  M_PI * pow(radius, 2) * height;
}

double surface(double radius, double height)
{
  return 2 * M_PI * pow(radius, 2) + 2 * M_PI * radius * height;
}

double milesToKilometers(double value)
{
  return value * 1.609344;
}

TEST_CASE("describe_factorial", "[factorial]")
{
  REQUIRE(factorial(0) == 1);
  REQUIRE(factorial(5) == 120);
  REQUIRE(isPrime(17) == true);
  REQUIRE(isPrime(19) == true);
  REQUIRE(isPrime(20) == false);
  REQUIRE(isPrime(3) == true);
  REQUIRE(fract(-6.1234) == Approx(-0.1234));
  REQUIRE(volume(2, 2) == Approx(25.133));
  REQUIRE(surface(2, 2) == Approx(50.265));
  REQUIRE(milesToKilometers(1) == Approx(1.609344));
}

TEST_CASE("checksum", "[checksum]")
{
  REQUIRE(checksum(94) == 13);
}

int main(int argc, char* argv[])
{
  return Catch::Session().run( argc, argv );
}

#endif // CATCH_CONFIG_RUNNER