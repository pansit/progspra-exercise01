#include <iostream>

/*
 * Zur Signatur gehört
 *  - der Rückgabetyp
 *  - beliebig viele Funktionsparameter
 * 
 * Der Gültigkeitsbereich einer Variablen beschreibt den Bereich in einem
 * Programm, in dem diese Variable existiert. Nur hier kann auf sie zugegriffen
 * werden. Üblicherweise ist das der Bereich, der von geschweiften Klammern
 * umschlossen wird.
 */
double sum(double a, double b)
{
  return a + b;
}

int square(int i)
{
  return i * i;
}

int main()
{
  for(int i = 0; i != 100; ++i)
  {
    std::cout << "i^2 = " << square(i) << std::endl;
    std::cout << "i+i = " << sum(i) << std::endl;
  }

  return 0;
}
