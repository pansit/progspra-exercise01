#include <iostream>
#include <string>

double milesToKilometers(double value)
{
  return value * 1.609344;
}

int main()
{
  std::cout << "Meilen eingeben: " << std::endl;
  double miles = 0.0;
  std::cin >> miles;
  std::cout << "Ergebnis: " << milesToKilometers(miles) << std::endl;

  return 0;
}