int main()
{
  /*
   * int ist ein Grunddatentyp der benutzt wird um Ganzzahlen zu speichern
   * int kann 2^32 Bit Zahlen speichern.
   */
  int       a   = 9;

  /*
   * bool ist ein Grunddatentyp in dem eine binäre Stelle gespeichert werden
   * kann, TRUE und FALSE.
   */
  bool      v   = false;

  /*
   * char steht für character. Mit diesem Datentyp können einzelne Buchstaben
   * speichern kann.
   */
  char      c   = 'a';

  /*
   * double kann Fließkommazahlen mit bis zu 6 Nachkommastellen
   * speichern
   */
  double    d   = 1.3;

  /*
   * int const speichert bei initialisierung einen konstanten Integer, der
   * der später nicht geändert werden kann
   */
  int const two = 2;

  /*
   * Diese Initialisierung führt eine Divisionsoperation aus und speichert diese
   * in der Variable e.
   */
  double    e   = a/two;

  /*
   * Diese Zuweisung wirft einen Fehler aus, weil versucht wird, die Variable d
   * (int const) zu ändern
   */
  two = d;

  return 0;
}