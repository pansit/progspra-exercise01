#Glossar
##C++
C++ ist eine von der ISO genormte Programmiersprache. Sie wurde ab 1979 von Bjarne Stroustrup bei AT&T als Erweiterung der Programmiersprache C entwickelt. C++ ermöglicht sowohl die effiziente und maschinennahe Programmierung als auch eine Programmierung auf hohem Abstraktionsniveau. Der Standard definiert auch eine Standardbibliothek, zu der verschiedene Implementierungen existieren.

##Quellcode:
Auch Quelltext genannt, ist der Text, der von einem Übersetzer (Compiler) in Maschinencode umgesetzt werden kann.
Der Quellcode beschreibt ein Programm oder Teile davon, sodass es menschenlesbar wird.

##Compiler:
Auch Übersetzer genannt, ist ein Programm, welches Quellcode in Machinencode umsetzt.

##Linker:
Der Linker ist ein Programm, dass Object und/oder Librarydateien eines Programms zu einer ausführbaren Datei zusammenfügt.

##Ausführbare Datei:
Mit einer ausführbaren Datei kann ein Programm von einem Nutzer gestartet (ausgeführt) werden.

##main():
In C++ benötigt jedes ausführbare Programm eine main()-Funktion von der aus Subroutinen gestartet werden.

## #include:
Mit der include Anweisung werden zusätzliche Funktionen von externen Bibliotheken oder Headerdateien in den Quellcode eingebunden. Somit werden Methoden aus diesen Dateien lokal nutzbar.

##Kommentar:
Mit der Kommentarfunktion können einzelne Zeilen oder Blöcke des Quellcodes für zusätzliche Notizen genutzt werden. Dies ist besonders nützlich wenn der Programmierer zusätzliche Beschreibungen für Methoden innerhalb seines Programms anbieten will.

##Header:
Der Header einer Quellcode Datei enthält üblicherweise Deklarationen zu Klassen und Methoden die noch definiert werden müssen.

##Programm:
Ein Programm ist eine Abfolge von Anweisungen, die abgearbeitet werden um ein Problem zu lösen (Kochrezept).

##Ausgabe:
Eine Kommandozeilenanwendung kann im Terminal einen Text anzeigen, der relevante Informationen zu einem Programm enthält.

##std::cout (Outputstream):
Eine Funktion, die Teil der iostream Library ist. Sie übernimmt die direkte Ausgabe auf der Konsole.

##std::cin (Inputstream):
Mit dieser Funktion kann ein String vom Benutzer entgegengenommen werden.

##<<
Der <<-Operator überführt einen weiteren Wert in den Outputstream.

##>>
Der >>-Operator überführt einen weiteren Wert in den Inputstream.

##Funktion:
Eine Funktion besteht aus der Signatur, dem Namen und einem Funktionsrumpf. Dieses Kontrukt übernimmt eine Teilaufgabe in einem Programm.

##Funktionssignatur:
Wenn eine Funktion überladen wird, kann sie der Compiler nur an hand der Signatur von Funktionen gleichen Namens unterscheiden. Die Signatur besteht aus Rückgabewert und Parameterliste.

##Deklaration:
Mit einer Deklaration wird dem Compiler mitgeteilt, dass eine Funktion oder Klasse existiert.

##Definition:
Eine bereits deklarierte Funktion oder Klasse wird in der Definition konkret implementiert.

##Typ:
Objekte in einem C++ Programm haben unterschiedliche Typen. Zum Beispiel ist eine Ganzzahl vom Typ int und eine Farbe kann vom Typ ColorRGB sein.

##Typkonvertierung:
In einigen Fällen kann ein Objekt des einen Typs in ein Objekt eines anderen Typs umgewandelt/konvertiert werden. So kann ein double in einen float umgewandelt werden. Allerdings geht dabei evtl. Präzision verloren.

##Variable:
In der Regel stellt die Variable eine Instanz eines Grunddatentyps, z.B. int, float, double, string etc. dar.

##Name:
Um Variablen und Objekte in Programmen zuornden zu können kann man ihnen einen Namen geben.

##Wert:
Grunddatentypen können einen Wert haben. Wenn sie keinen Wert haben, wurden sie noch nicht initialisiert sondern nur deklariert -> wiederspricht der Konvention RAII

##Initialisierung:
Wenn einer Variable ein Wert zugewiesen wird, wird sie initialisiert oder zum ersten mal verfügbar gemacht.

##Zuweisung:
Einer Variable kann ein neuer Wert zugewiesen werden, indem sie zum ersten Mal einen Wert erhält (Initialisierung) oder wenn der vorhandene Wert überschrieben wird.

##const:
Das Schlüsselwort const kann bei Variablen verwendet werden wenn sie nicht verändert werden können sollen.
Bei Funktionen wird es benutzt um sicherzustellen, dass bei der Verwendung von Übergabeparametern, selbe nicht verändert werden können.

##Gültigkeitsbereich:
Der Gültigkeitsbereich ist jener Bereich in dem ein Objekt verfügbar ist. Im Normalfall ist das genau der Bereich zwischen zwei geschweiften Klammern in dem das Objekt auch erzeugt wurde.