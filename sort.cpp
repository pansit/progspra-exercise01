#include <iostream>
#include <algorithm>
#include <vector>

void swap(int& a, int& b)
{
  int t = a;
  a = b;
  b = t;
}

void bubble(int a, int b, int c)
{
  if(a > b)
    swap(a,b);
  if(b > c)
    swap(b,c);
  if(a > b)
    swap(a,b);
  std::cout << a << std::endl;
  std::cout << b << std::endl;
  std::cout << c << std::endl;
}

void sort(int a, int b, int c)
{
  std::vector<int> v;
  v.push_back(a);
  v.push_back(b);
  v.push_back(c);
  std::sort(v.begin(), v.end());
  for(auto i : v)
    std::cout << i << std::endl;
}

int main()
{
  std::cout << "3 Ganzzahlen eingeben:" << std::endl;
  int a,b,c;
  std::cin >> a;
  std::cin >> b;
  std::cin >> c;

  sort(a,b,c);
  //bubble(a,b,c);

  return 0;
}