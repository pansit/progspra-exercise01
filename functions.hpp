#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

unsigned factorial(unsigned n);
unsigned checksum(unsigned n);
bool isPrime(int value);
double frac(double value);
double milesToKilometers(double value);

#endif //FUNCTIONS_HPP